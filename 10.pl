use strict;
use warnings;
use Time::HiRes qw( time );
my $start = time();
use List::Util qw |first|;

my $infile = $ARGV[0];
die "\ninput file not specified; borking.\n\n" unless defined($infile);

my @ops = ('(','{','[','<');
my @cls = (')','}',']','>');
my @scores = (3,1197,57,25137);
my @comscores = (1,3,2,4);
my $totalscore = 0;
my @autoscores;

open(my $inf, '<', $infile) || die "cannot open file $infile: $!\n";

while(<$inf>) { 
	chomp;
	my @c = split //;
	my $lastop;
	my $lastopidx;
	my $corr = 0;
	my @openstack = ();

	for (my $i=0; $i<=$#c; $i++) {
		next unless ($c[$i] ne '.');
		# chunk opens
		if( grep { $c[$i] eq $_ } @ops ) {
			$lastop = $c[$i];
			$lastopidx = $i;
			push @openstack, $lastop;
		}
		# chunk closes
		elsif( grep { $c[$i] eq $_ } @cls ) {
			my $oi = first { $ops[$_] eq $lastop } 0..$#ops;
			my $ci = first { $cls[$_] eq $c[$i] } 0..$#cls;
			if ($ci == $oi) {
				$c[$i] = '.';
				$c[$lastopidx] = '.';
				@openstack = ();
				$i = 0;
				redo;
			} else {
				# corrupted
				$totalscore += $scores[$ci];
				$corr++;
				last;
			}
		}	
	}
	my $checkline = join '', @c;
	$checkline =~ s/\.//g;
	if ( (!$corr) && (length($checkline)) ) {
		# incomplete
		my $completion = '';
		my $completionScore = 0;
		foreach my $thang (reverse @openstack) {
			my $zoi = first { $ops[$_] eq $thang } 0..$#ops;
			$completion .= $cls[$zoi];
			$completionScore = ($completionScore * 5) + $comscores[$zoi];
			}
		# print "Completion string = $completion\t";
		push @autoscores, $completionScore;
	}
}

close $inf;
printf "Part1 Score =\t%d\n", $totalscore;
@autoscores = sort { $a <=> $b } @autoscores;
printf "Part2 Score = \t";
print $autoscores[int @autoscores/2];

printf("\n== Execution Time: %0.02f s ==\n\n", time() - $start);
exit;
